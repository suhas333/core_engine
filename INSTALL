Installation Instructions
*************************

Copyright (C)  2007, 2008, 2009
Comet Media Foundation.



This file is free documentation; Comet Media Foundation gives
unlimited permission to copy, distribute and modify it.
Under the terms of GNU General Public License published by the Free Software Foundation, either version 3 or any later version at your choice.

*WARNING*

Comet Media Foundation or the author of this document disclaims all warranties including but not limited the implied warrenty of merchantability and fitness for a particular purpose.
FOLLOW THE INSTRUCTIONS AT YOUR OWN RISK!
NOTE:
These set of instructions are meant for those who download the source and run it directly.
The actual setup script will perform all the given activities for an end user using GNUKhata for production.

You can also install gnukhata from debian packages available from,
http://people.debian.org/~praveen/gnukhata/ (follow instructions in the README
file there)

Basic Installation
==================
Gnukhata depends on the following components and they should be installed for the software to be functional.
For the Core engine:
	* postgresql for the database, version 9.x
	* sqlite3 needed for suplimentary data
	* python version 2.7 recommended for stability
	* python-psycopg2 for the database connectivity and interfacing
	between python and postgresql.
	* python-sqlalchemy 0.7.x for stored procedures execution
	* python-twisted for the xmlrpc service

For checkout of source code:
* git for getting (pulling) the code and committing changes.

1. Install dependencies:
$ sudo apt-get install python-psycopg2 postgresql python-twisted python-dateutil

You will need python-sqlalchemy 0.7.x version for stored procedures to work.
We are working with sqlalchemy upstream to make gnukhata work with newer
versions of sqlalchemy. You can see the status of this issue at,
https://bitbucket.org/zzzeek/sqlalchemy/issue/3103/nosuchcolumnerror-could-not-locate-column

You can install python-sqlalchemy7 from,
http://people.debian.org/~praveen/gnukhata/ (follow instructions in the README
file there)

2. Copy conf/gnukhata.conf to /etc,
$ sudo cp conf/gnukhata.conf /etc

3. Use scripts/adduser.sh to create a separate user for core_engine service

$ sudo ./scripts/adduser.sh

4. Create a directory for storing gnukhata.xml
$ sudo mkdir -p /var/lib/gnukhata
$ sudo chown gnukhata: /var/lib/gnukhata

5. Create a database user and grant required privileges

$ sudo -u postgres createuser gnukhata
$ sudo ./scripts/grantpriv.sh

GNUKhata server is ready to be used.

To start GNUKhata type sudo -u gnukhata ./gkstart from the core_engine directory.
